/*
 * Copyright (C) 2015 University of Chicago.
 * See COPYRIGHT notice in top-level directory.
 *
 */

#define __USE_GNU
#include <dlfcn.h>

#define BBIO_FORWARD_DECL(name,ret,args) \
  ret (*__real_ ## name)args = NULL

#define BBIO_DECL(__name) __name

#define MAP_OR_FAIL(func) \
    if (!(__real_ ## func)) \
    { \
        *(void **) (&__real_ ## func) = dlsym(RTLD_NEXT, #func); \
        if(!(__real_ ## func)) { \
           fprintf(stderr, "BBIO failed to map symbol: %s\n", #func); \
           exit(1); \
       } \
    }

