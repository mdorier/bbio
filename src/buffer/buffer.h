/*
 * Copyright (C) 2015 University of Chicago.
 * See COPYRIGHT notice in top-level directory.
 *
 */

#ifndef BBIO_BUFFER_H
#define BBIO_BUFFER_H
/******************************************************************************
*standard include files 
******************************************************************************/
#include "bbio-config.h"
#include "bbio.h"

#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************
*Function declarations
******************************************************************************/
int bbio_init(const char* path, size_t buf_size);

int bbio_finalize();

int bbio_enable(int fd);

int bbio_disable(int fd);

int bbio_flush(int fd);

int bbio_on_flush(int fd, BBIO_Callback cb);

int bbio_buffer_write_begin(int fd, const char* buf, size_t count);

int bbio_buffer_writev_begin(int fd, const struct iovec *iov, size_t iovcnt);

int bbio_buffer_pwrite_begin(int fd, const char *buf, size_t count, off64_t offset);

int bbio_buffer_close_or_sync_begin(int fd, int remove);

int bbio_buffer_create_or_open_begin(const char* path);

int bbio_buffer_create_or_open_end(int ret);

off64_t bbio_buffer_seek_begin(int fd, off64_t offset, int whence);

#ifdef __cplusplus
}
#endif

#endif /* IOLOGY_BUFFER_H */
