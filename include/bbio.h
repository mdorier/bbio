/*
 * Copyright (C) 2015 University of Chicago.
 * See COPYRIGHT notice in top-level directory.
 *
 */

#ifndef BBIO_H
#define BBIO_H

#include <unistd.h>

#ifdef __cplusplus
extern "C" {
#endif

#define BBIO_OK 	 0
#define BBIO_ERROR 	-1

#define BBIO_TRUE 	1
#define BBIO_FALSE	0

typedef void (*BBIO_Callback)(int fd);

int BBIO_Init(const char* path, size_t max_size);

int BBIO_Finalize();

int BBIO_Enable(int file);

int BBIO_Disable(int file);

int BBIO_Flush(int file);

int BBIO_On_flush(int fd, BBIO_Callback cb);

#ifdef __cplusplus
}
#endif

#endif
