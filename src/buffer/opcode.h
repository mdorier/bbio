/*
 * Copyright (C) 2015 University of Chicago.
 * See COPYRIGHT notice in top-level directory.
 *
 */

#ifndef BBIO_OPCODE_H
#define BBIO_OPCODE_H

namespace bbio {

#define	BBIO_WRITE	1
#define	BBIO_PWRITE	2
#define	BBIO_SEEK	3

}

#endif
