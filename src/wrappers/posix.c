/*
 * Copyright (C) 2015 University of Chicago.
 * See COPYRIGHT notice in top-level directory.
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/uio.h>
#include <sys/mman.h>
#include <search.h>
#include <assert.h>
#include <libgen.h>
#include <limits.h>
#include <aio.h>
#include <stdlib.h>

#include "bbio-config.h"
#include "wrappers/util.h"
#include "buffer/macros.h"

BBIO_FORWARD_DECL(mkstemp, int, (char *template));
BBIO_FORWARD_DECL(mkostemp, int, (char *template, int flags));
BBIO_FORWARD_DECL(mkstemps, int, (char *template, int suffixlen));
BBIO_FORWARD_DECL(mkostemps, int, (char *template, int suffixlen, int flags));
BBIO_FORWARD_DECL(creat, int, (const char* path, mode_t mode));
BBIO_FORWARD_DECL(creat64, int, (const char* path, mode_t mode));
BBIO_FORWARD_DECL(open, int, (const char *path, int flags, ...));
BBIO_FORWARD_DECL(open64, int, (const char *path, int flags, ...));
BBIO_FORWARD_DECL(close, int, (int fd));
BBIO_FORWARD_DECL(write, ssize_t, (int fd, const void *buf, size_t count));
BBIO_FORWARD_DECL(read, ssize_t, (int fd, void *buf, size_t count));
BBIO_FORWARD_DECL(lseek, off_t, (int fd, off_t offset, int whence));
BBIO_FORWARD_DECL(lseek64, off64_t, (int fd, off64_t offset, int whence));
BBIO_FORWARD_DECL(pread, ssize_t, (int fd, void *buf, size_t count, off_t offset));
BBIO_FORWARD_DECL(pread64, ssize_t, (int fd, void *buf, size_t count, off64_t offset));
BBIO_FORWARD_DECL(pwrite, ssize_t, (int fd, const void *buf, size_t count, off_t offset));
BBIO_FORWARD_DECL(pwrite64, ssize_t, (int fd, const void *buf, size_t count, off64_t offset));
BBIO_FORWARD_DECL(readv, ssize_t, (int fd, const struct iovec *iov, int iovcnt));
BBIO_FORWARD_DECL(writev, ssize_t, (int fd, const struct iovec *iov, int iovcnt));
BBIO_FORWARD_DECL(__fxstat, int, (int vers, int fd, struct stat *buf));
BBIO_FORWARD_DECL(__fxstat64, int, (int vers, int fd, struct stat64 *buf));
BBIO_FORWARD_DECL(__lxstat, int, (int vers, const char* path, struct stat *buf));
BBIO_FORWARD_DECL(__lxstat64, int, (int vers, const char* path, struct stat64 *buf));
BBIO_FORWARD_DECL(__xstat, int, (int vers, const char* path, struct stat *buf));
BBIO_FORWARD_DECL(__xstat64, int, (int vers, const char* path, struct stat64 *buf));
BBIO_FORWARD_DECL(mmap, void*, (void *addr, size_t length, int prot, int flags, int fd, off_t offset));
BBIO_FORWARD_DECL(mmap64, void*, (void *addr, size_t length, int prot, int flags, int fd, off64_t offset));
BBIO_FORWARD_DECL(fopen, FILE*, (const char *path, const char *mode));
BBIO_FORWARD_DECL(fopen64, FILE*, (const char *path, const char *mode));
BBIO_FORWARD_DECL(fclose, int, (FILE *fp));
BBIO_FORWARD_DECL(fread, size_t, (void *ptr, size_t size, size_t nmemb, FILE *stream));
BBIO_FORWARD_DECL(fwrite, size_t, (const void *ptr, size_t size, size_t nmemb, FILE *stream));
BBIO_FORWARD_DECL(fseek, int, (FILE *stream, long offset, int whence));
BBIO_FORWARD_DECL(fsync, int, (int fd));
BBIO_FORWARD_DECL(fdatasync, int, (int fd));
BBIO_FORWARD_DECL(aio_read, int, (struct aiocb *aiocbp));
BBIO_FORWARD_DECL(aio_read64, int, (struct aiocb64 *aiocbp));
BBIO_FORWARD_DECL(aio_write, int, (struct aiocb *aiocbp));
BBIO_FORWARD_DECL(aio_write64, int, (struct aiocb64 *aiocbp));
BBIO_FORWARD_DECL(lio_listio, int, (int mode, struct aiocb *const aiocb_list[], int nitems, struct sigevent *sevp));
BBIO_FORWARD_DECL(lio_listio64, int, (int mode, struct aiocb64 *const aiocb_list[], int nitems, struct sigevent *sevp));
BBIO_FORWARD_DECL(aio_return, ssize_t, (struct aiocb *aiocbp));
BBIO_FORWARD_DECL(aio_return64, ssize_t, (struct aiocb64 *aiocbp));

int BBIO_DECL(close)(int fd)
{
    int ret;
    MAP_OR_FAIL(close);
    bbio_close_begin(fd);
    ret = __real_close(fd);
    bbio_close_end(ret);
    return ret;
}

int BBIO_DECL(fclose)(FILE *fp)
{
    int ret;
    MAP_OR_FAIL(fclose);
    bbio_fclose_begin(fp);
    ret = __real_fclose(fp);
    bbio_fclose_end(ret);
    return ret;
}


int BBIO_DECL(fsync)(int fd)
{
    int ret;
    MAP_OR_FAIL(fsync);
    bbio_fsync_begin(fd);
    ret = __real_fsync(fd);
    bbio_fsync_end(ret);
    return(ret);
}

int BBIO_DECL(fdatasync)(int fd)
{
    int ret;
    MAP_OR_FAIL(fdatasync);
    bbio_fdatasync_begin(fd);
    ret = __real_fdatasync(fd);
    bbio_fdatasync_end(ret);
    return(ret);
}


void* BBIO_DECL(mmap64)(void *addr, size_t length, int prot, int flags,
    int fd, off64_t offset)
{
    void* ret;
    MAP_OR_FAIL(mmap64);
    bbio_mmap64_begin(addr, length, prot, flags, fd, offset);
    ret = __real_mmap64(addr, length, prot, flags, fd, offset);
    bbio_mmap64_end(ret);
    return(ret);
}


void* BBIO_DECL(mmap)(void *addr, size_t length, int prot, int flags,
    int fd, off_t offset)
{
    void* ret;
    MAP_OR_FAIL(mmap);
    bbio_mmap_begin(addr, length, prot, flags, fd, offset);
    ret = __real_mmap(addr, length, prot, flags, fd, offset);
    bbio_mmap_end(ret);
    return(ret);
}

int BBIO_DECL(creat)(const char* path, mode_t mode)
{
    int ret;
    MAP_OR_FAIL(creat);
    bbio_creat_begin(path,mode);
    ret = __real_creat(path, mode);
    bbio_creat_end(ret);
    return(ret);
}

int BBIO_DECL(mkstemp)(char* template)
{
    int ret;
    MAP_OR_FAIL(mkstemp);
    bbio_mkstemp_begin(template);
    ret = __real_mkstemp(template);
    bbio_mkstemp_end(ret);
    return(ret);
}

int BBIO_DECL(mkostemp)(char* template, int flags)
{
    int ret;
    MAP_OR_FAIL(mkostemp);
    bbio_mkostemp_begin(template,flags);
    ret = __real_mkostemp(template, flags);
    bbio_mkostemp_end(ret);
    return(ret);
}


int BBIO_DECL(mkstemps)(char* template, int suffixlen)
{
    int ret;
    MAP_OR_FAIL(mkstemps);
    bbio_mkstemps_begin(template, suffixlen);
    ret = __real_mkstemps(template, suffixlen);
    bbio_mkstemps_end(ret);
    return(ret);
}


int BBIO_DECL(mkostemps)(char* template, int suffixlen, int flags)
{
    int ret;
    MAP_OR_FAIL(mkostemps);
    bbio_mkostemps_begin(template, suffixlen, flags);
    ret = __real_mkostemps(template, suffixlen, flags);
    bbio_mkostemps_end(ret);
    return(ret);
}

int BBIO_DECL(creat64)(const char* path, mode_t mode)
{
    int ret;
    MAP_OR_FAIL(creat64);
    bbio_creat64_begin(path, mode);
    ret = __real_creat64(path, mode);
    bbio_creat64_end(ret);
    return(ret);
}

int BBIO_DECL(open64)(const char* path, int flags, ...)
{
    int mode = 0;
    int ret;
    MAP_OR_FAIL(open64);
    if (flags & O_CREAT) 
    {
        va_list arg;
        va_start(arg, flags);
        mode = va_arg(arg, int);
        va_end(arg);

	bbio_open64_begin(path, flags, mode);
        ret = __real_open64(path, flags, mode);
	bbio_open64_end(ret);
    }
    else
    {
	bbio_open64_begin(path, flags, mode);
        ret = __real_open64(path, flags);
	bbio_open64_end(ret);
    }
    return(ret);
}

int BBIO_DECL(open)(const char *path, int flags, ...)
{
    int mode = 0;
    int ret;

    MAP_OR_FAIL(open);
    if (flags & O_CREAT) 
    {
        va_list arg;
        va_start(arg, flags);
        mode = va_arg(arg, int);
        va_end(arg);

	bbio_open_begin(path, flags, mode);
        ret = __real_open(path, flags, mode);
	bbio_open_end(ret);
    }
    else
    {
	bbio_open_begin(path, flags, mode);
        ret = __real_open(path, flags);
	bbio_open_end(ret);
    }
    return(ret);
}

FILE* BBIO_DECL(fopen64)(const char *path, const char *mode)
{
    FILE* ret;
    MAP_OR_FAIL(fopen64);
    bbio_fopen64_begin(path, mode);
    ret = __real_fopen64(path, mode);
    bbio_fopen64_end(ret);
    return(ret);
}

FILE* BBIO_DECL(fopen)(const char *path, const char *mode)
{
    FILE* ret;
    MAP_OR_FAIL(fopen);
    bbio_fopen_begin(path, mode);
    ret = __real_fopen(path, mode);
    bbio_fopen_end(ret);
    return(ret);
}

int BBIO_DECL(__xstat64)(int vers, const char *path, struct stat64 *buf)
{
    int ret;
    MAP_OR_FAIL(__xstat64);
    bbio_xstat64_begin(vers,path,buf);
    ret = __real___xstat64(vers, path, buf);
    bbio_xstat64_end(ret);
    return(ret);
}

int BBIO_DECL(__lxstat64)(int vers, const char *path, struct stat64 *buf)
{
    int ret;
    MAP_OR_FAIL(__lxstat64);
    bbio_lxstat64_begin(vers,path,buf);
    ret = __real___lxstat64(vers, path, buf);
    bbio_lxstat64_end(ret);
    return(ret);
}

int BBIO_DECL(__fxstat64)(int vers, int fd, struct stat64 *buf)
{
    int ret;
    MAP_OR_FAIL(__fxstat64);
    bbio_fxstat64_begin(vers,fd,buf);
    ret = __real___fxstat64(vers, fd, buf);
    bbio_fxstat64_end(ret);
    return(ret);
}


int BBIO_DECL(__xstat)(int vers, const char *path, struct stat *buf)
{
    int ret;
    MAP_OR_FAIL(__xstat);
    bbio_xstat_begin(vers,path,buf);
    ret = __real___xstat(vers, path, buf);
    bbio_xstat_end(ret);
    return(ret);
}

int BBIO_DECL(__lxstat)(int vers, const char *path, struct stat *buf)
{
    int ret;
    MAP_OR_FAIL(__lxstat);
    bbio_lxstat_begin(vers,path,buf);
    ret = __real___lxstat(vers, path, buf);
    bbio_lxstat_end(ret);
    return(ret);
}

int BBIO_DECL(__fxstat)(int vers, int fd, struct stat *buf)
{
    int ret;
    MAP_OR_FAIL(__fxstat);
    bbio_fxstat_begin(vers,fd,buf);
    ret = __real___fxstat(vers, fd, buf);
    bbio_fxstat_end(ret);
    return(ret);
}

ssize_t BBIO_DECL(pread64)(int fd, void *buf, size_t count, off64_t offset)
{
    ssize_t ret;
    MAP_OR_FAIL(pread64);
    bbio_pread64_begin(fd, buf,count,offset);
    ret = __real_pread64(fd, buf, count, offset);
    bbio_pread64_end(ret);
    return(ret);
}

ssize_t BBIO_DECL(pread)(int fd, void *buf, size_t count, off_t offset)
{
    ssize_t ret;
    MAP_OR_FAIL(pread);
    bbio_pread_begin(fd,buf,count,offset);
    ret = __real_pread(fd, buf, count, offset);
    bbio_pread_end(ret);
    return(ret);
}


ssize_t BBIO_DECL(pwrite)(int fd, const void *buf, size_t count, off_t offset)
{
    ssize_t ret;
    MAP_OR_FAIL(pwrite);
    bbio_pwrite_begin(fd,buf,count,offset);
    ret = __real_pwrite(fd, buf, count, offset);
    bbio_pwrite_end(ret);
    return(ret);
}

ssize_t BBIO_DECL(pwrite64)(int fd, const void *buf, size_t count, off64_t offset)
{
    ssize_t ret;
    MAP_OR_FAIL(pwrite64);
    bbio_pwrite64_begin(fd,buf,count,offset);
    ret = __real_pwrite64(fd, buf, count, offset);
    bbio_pwrite64_end(ret);
    return(ret);
}

ssize_t BBIO_DECL(readv)(int fd, const struct iovec *iov, int iovcnt)
{
    ssize_t ret;
    MAP_OR_FAIL(readv);
    bbio_readv_begin(fd, iov, iovcnt);
    ret = __real_readv(fd, iov, iovcnt);
    bbio_readv_end(ret);
    return(ret);
}

ssize_t BBIO_DECL(writev)(int fd, const struct iovec *iov, int iovcnt)
{
    ssize_t ret;
    MAP_OR_FAIL(writev);
    bbio_writev_begin(fd, iov, iovcnt);
    ret = __real_writev(fd, iov, iovcnt);
    bbio_writev_end(ret);
    return(ret);
}

size_t BBIO_DECL(fread)(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t ret;
    MAP_OR_FAIL(fread);
    bbio_fread_begin(ptr, size, nmemb, stream);
    ret = __real_fread(ptr, size, nmemb, stream);
    bbio_fread_end(ret);
    return(ret);
}

ssize_t BBIO_DECL(read)(int fd, void *buf, size_t count)
{
    ssize_t ret;
    MAP_OR_FAIL(read);
    bbio_read_begin(fd, buf, count);
    ret = __real_read(fd, buf, count);
    bbio_read_end(ret);
    return(ret);
}

ssize_t BBIO_DECL(write)(int fd, const void *buf, size_t count)
{
    ssize_t ret;
    MAP_OR_FAIL(write);
    bbio_write_begin(fd, buf, count);
    ret = __real_write(fd, buf, count);
    bbio_write_end(fd);
    return(ret);
}

size_t BBIO_DECL(fwrite)(const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t ret;
    MAP_OR_FAIL(fwrite);
    bbio_fwrite_begin(ptr, size, nmemb, stream);
    ret = __real_fwrite(ptr, size, nmemb, stream);
    bbio_fwrite_end(ret);
    return(ret);
}

off64_t BBIO_DECL(lseek64)(int fd, off64_t offset, int whence)
{
    off64_t ret;
    MAP_OR_FAIL(lseek64);
    bbio_lseek64_begin(fd, offset, whence);
    ret = __real_lseek64(fd, offset, whence);
    bbio_lseek64_end(ret);
    return(ret);
}

off_t BBIO_DECL(lseek)(int fd, off_t offset, int whence)
{
    off_t ret;
    MAP_OR_FAIL(lseek);
    bbio_lseek_begin(fd, offset, whence);
    ret = __real_lseek(fd, offset, whence);
    bbio_lseek_end(ret);
    return(ret);
}

ssize_t BBIO_DECL(aio_return64)(struct aiocb64 *aiocbp)
{
    int ret;
    MAP_OR_FAIL(aio_return64);
    bbio_aio_return64_begin(aiocbp);
    ret = __real_aio_return64(aiocbp);
    bbio_aio_return64_end(ret);
    return(ret);
}

ssize_t BBIO_DECL(aio_return)(struct aiocb *aiocbp)
{
    int ret;
    MAP_OR_FAIL(aio_return);
    bbio_aio_return_begin(aiocbp);
    ret = __real_aio_return(aiocbp);
    bbio_aio_return64_end(ret);
    return(ret);
}

int BBIO_DECL(lio_listio)(int mode, struct aiocb *const aiocb_list[],
    int nitems, struct sigevent *sevp)
{
    int ret;
    MAP_OR_FAIL(lio_listio);
    bbio_lio_listio_begin(mode, aiocb_list, nitems, sevp);
    ret = __real_lio_listio(mode, aiocb_list, nitems, sevp);
    bbio_lio_listio_end(ret);
    return(ret);
}

int BBIO_DECL(lio_listio64)(int mode, struct aiocb64 *const aiocb_list[],
    int nitems, struct sigevent *sevp)
{
    int ret;
    MAP_OR_FAIL(lio_listio);
    bbio_lio_listio64_begin(mode, aiocb_list, nitems, sevp);
    ret = __real_lio_listio64(mode, aiocb_list, nitems, sevp);
    bbio_lio_listio64_end(ret);
    return(ret);
}

int BBIO_DECL(aio_write64)(struct aiocb64 *aiocbp)
{
    int ret;
    MAP_OR_FAIL(aio_write64);
    bbio_aio_write64_begin(aiocbp);
    ret = __real_aio_write64(aiocbp);
    bbio_aio_write64_end(ret);
    return(ret);
}

int BBIO_DECL(aio_write)(struct aiocb *aiocbp)
{
    int ret;
    MAP_OR_FAIL(aio_write);
    bbio_aio_write_begin(aiocbp);
    ret = __real_aio_write(aiocbp);
    bbio_aio_write_end(ret);
    return(ret);
}

int BBIO_DECL(aio_read64)(struct aiocb64 *aiocbp)
{
    int ret;
    MAP_OR_FAIL(aio_read64);
    bbio_aio_read64_begin(aiocbp);
    ret = __real_aio_read64(aiocbp);
    bbio_aio_read_end(ret);
    return(ret);
}

int BBIO_DECL(aio_read)(struct aiocb *aiocbp)
{
    int ret;
    MAP_OR_FAIL(aio_read);
    bbio_aio_read_begin(aiocbp);
    ret = __real_aio_read(aiocbp);
    bbio_aio_read_end(ret);
    return(ret);
}

int BBIO_DECL(fseek)(FILE *stream, long offset, int whence)
{
    int ret;
    MAP_OR_FAIL(fseek);
    bbio_fseek_begin(stream, offset, whence);
    ret = __real_fseek(stream, offset, whence);
    bbio_fseek_end(ret);
    return(ret);
}

