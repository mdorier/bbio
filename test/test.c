#include <stdio.h>
#include "bbio.h"

void my_callback(int fd) {
	printf("Before flushing buffer for file descriptor %d\n", fd);
}

int main ()
{
	// initialize BBIO (looup wrapper symbols)
	BBIO_Init("/tmp",1024);
	FILE * pFile;
	char buffer[] = { 'x' , 'y' , 'z' };
	pFile = fopen ("myfile.txt", "wb");
	// attach a callback to the file descriptor
	BBIO_On_flush(fileno(pFile), my_callback);
	// the following write is buffered
	fwrite(buffer , sizeof(char), sizeof(buffer), pFile);
	BBIO_Disable(fileno(pFile));
	// the following write is not buffered
	fwrite(buffer , sizeof(char), sizeof(buffer), pFile);
	BBIO_Enable(fileno(pFile));
	// the following write is buffered
	fwrite(buffer , sizeof(char), sizeof(buffer), pFile);
	BBIO_Flush(fileno(pFile));
	// the following writes are buffered
	int i;
	for(i = 0; i < 512; i++) {
		fwrite(buffer , sizeof(char), sizeof(buffer), pFile);
	}
	// this triggers a flush
	fclose(pFile);
	// finalize BBIO
	BBIO_Finalize();
	return 0;
}
